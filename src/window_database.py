import mysql.connector
from mysql.connector import errorcode
from PySide6 import QtCore
from PySide6.QtWidgets import (
	QApplication,
	QMainWindow,
	QStackedLayout,
	QFrame,
	QVBoxLayout,
	QLabel,
	QPushButton,
	QWidget,
	QTableWidgetItem,
	QTableWidget,
	QCompleter
)
from window_main import window_main
from window_insert_sell import window_insert_sell
from window_pending import window_pending_main
from window_error_db import window_error_mysql
from window_logs import window_logs
from window_products import window_products
from dictionary import connection_config_dict

header_columns = [
	"ID Venta",
	"Nombre",
	"Apellido",
	"Producto",
	"Precio",
	"Cantidad",
	"Total",
	"Tipo de Pago",
	"Pagado",
	"Vuelto",
	"Producto Dado",
	"Cambio Dado",
	"Hora",
	"Fecha"
]

class window_sql (QMainWindow):
	def __init__ (self):
		super().__init__()

		self.setWindowTitle("Mi Primer Emprendimiento")

		self.layout_sql = QStackedLayout()

		self.layout_main = window_main()
		self.layout_insert_sell = window_insert_sell()
		self.layout_pending = window_pending_main()
		self.layout_logs = window_logs()
		self.layout_products = window_products()
		self.layout_error_db = window_error_mysql()

		list_widgets = [
			self.layout_main,
			self.layout_insert_sell,
			self.layout_pending,
			self.layout_logs,
			self.layout_products,
			self.layout_error_db
		]

		for i in list_widgets:
			self.layout_sql.addWidget(i)

		self.layout_main.button_insert_sell.clicked.connect(self.change_window_insert_sell)
		self.layout_main.button_pending.clicked.connect(self.change_window_pending)
		self.layout_main.button_all_log.clicked.connect(self.change_window_logs)
		self.layout_main.button_products.clicked.connect(self.change_window_insert_product)
		self.layout_main.button_exit.clicked.connect(self.close)

		self.layout_insert_sell.layout_sell.button_return.clicked.connect(self.change_window_main)
		self.layout_pending.layout_pending_main.button_return.clicked.connect(self.change_window_main)
		self.layout_logs.layout_logs.button_return.clicked.connect(self.change_window_main)
		self.layout_products.layout_main.button_return.clicked.connect(self.change_window_main)
		self.layout_error_db.button_exit.clicked.connect(self.close)

		self.layout_insert_sell.layout_sell.button_insert.clicked.connect(self.window_insert_sell)
		self.layout_insert_sell.layout_sell.button_new_sell.clicked.connect(self.clear_window_insert)
		self.layout_insert_sell.layout_sell.button_logs.clicked.connect(self.change_window_logs)

		self.layout_pending.layout_pending_main.button_pending_change.clicked.connect(self.window_pending_money)
		self.layout_pending.layout_pending_main.button_pending_product.clicked.connect(self.window_pending_product)
		self.layout_pending.layout_pending_main.button_pending_all.clicked.connect(self.window_pending_all)

		self.layout_products.layout_main.button_see.clicked.connect(self.window_products_see)
		self.layout_products.layout_insert.button_insert.clicked.connect(self.window_products_insert)

		self.main_widget = QWidget()
		self.main_widget.setLayout(self.layout_sql)
		self.setCentralWidget(self.main_widget)

	####################################
	####	Cambios de Ventana		####
	####################################

	def change_window_main (self):
		self.layout_sql.setCurrentIndex(0)

	def change_window_insert_sell(self):
		self.layout_sql.setCurrentIndex(1)
		self.data_filling_process()

	def change_window_pending(self):
		self.layout_sql.setCurrentIndex(2)

	def change_window_logs(self):
		self.layout_sql.setCurrentIndex(3)
		self.window_logs_all()

	def change_window_insert_product(self):
		self.layout_sql.setCurrentIndex(4)
	
	def change_window_error_db(self):
		self.layout_sql.setCurrentIndex(5)

	############################################
	####	Ventana Inserción de Ventas		####
	############################################

	def clear_window_insert(self):
		self.layout_insert_sell.layout_sell.line_edit_nombre.setText('')
		self.layout_insert_sell.layout_sell.line_edit_apellido.setText('')
		self.layout_insert_sell.layout_sell.line_edit_product_name.setText('')
		self.layout_insert_sell.layout_sell.spin_box_to_buy.setValue(0)
		self.layout_insert_sell.layout_sell.combo_box_payment_method.setCurrentIndex(0)
		self.layout_insert_sell.layout_sell.double_spin_box_received_money.setValue(0)

		self.layout_insert_sell.layout_sell.check_box_product_delivered.setChecked(True)
		self.layout_insert_sell.layout_sell.check_box_change_delivered.setChecked(True)
		self.layout_insert_sell.layout_sell.label_inserted.setHidden(True)
		self.layout_insert_sell.layout_sell.label_change_total.setHidden(True)

	def data_filling_process(self):
		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on data_filling_process")

			# Para el Nombre
			cursor.execute("SELECT Nombre FROM Definitivo GROUP BY Nombre")
			nombres_clientes = cursor.fetchall()
			size_nombres_clientes = len(nombres_clientes)
			items = []
			for i in range(size_nombres_clientes):
				items.insert(i, nombres_clientes[i][0])
			completer = QCompleter(items)
			self.layout_insert_sell.layout_sell.line_edit_nombre.setCompleter(completer)

			# Para el Apellido
			cursor.execute("SELECT Apellido FROM Definitivo GROUP BY Apellido")
			apellidos_clientes = cursor.fetchall()
			size_apellidos_clientes = len(apellidos_clientes)
			items = []
			for i in range(size_apellidos_clientes):
				items.insert(i, apellidos_clientes[i][0])
			completer = QCompleter(items)
			self.layout_insert_sell.layout_sell.line_edit_apellido.setCompleter(completer)

			# Para los Productos
			cursor.execute("SELECT nombre FROM Productos")
			nombres_productos = cursor.fetchall()
			size_nombre_productos = len(nombres_productos)
			items = []
			for i in range(size_nombre_productos):
				items.insert(i, nombres_productos[i][0].lower())
			completer = QCompleter(items)
			self.layout_insert_sell.layout_sell.line_edit_product_name.setCompleter(completer)

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()
			print(error_message)

		# Para el método de pago
		items = ['Efectivo', 'Yappy', 'Al Contado']
		self.layout_insert_sell.layout_sell.combo_box_payment_method.addItems(items)

	def window_insert_sell(self):
		add_sell = '''
			INSERT INTO Registros (nombre_cliente, apellido_cliente, id_producto, cantidad, tipo_pago, pagado, producto_entregado, cambio_entregado)
			VALUES	(%s, %s, %s, %s, %s, %s, %s, %s)
		'''

		nombre = self.layout_insert_sell.layout_sell.line_edit_nombre.text()
		nombre = nombre.capitalize()
		apellido = self.layout_insert_sell.layout_sell.line_edit_apellido.text()
		apellido = apellido.capitalize()
		producto = self.layout_insert_sell.layout_sell.line_edit_product_name.text()
		producto = producto.capitalize()
		cantidad = int(self.layout_insert_sell.layout_sell.spin_box_to_buy.text())
		payment = self.layout_insert_sell.layout_sell.combo_box_payment_method.currentText()
		money = float(self.layout_insert_sell.layout_sell.double_spin_box_received_money.text())
		if (self.layout_insert_sell.layout_sell.check_box_product_delivered.isChecked()):
			delivered = "Sí"
		else:
			delivered = "No"
		if (self.layout_insert_sell.layout_sell.check_box_change_delivered.isChecked()):
			change = "Sí"
		else:
			change = "No"

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("you are on window_insert_sell")

			product_id = cursor.callproc('obtener_producto_id', [producto, 0])
			#print(product_id)

			sell_items = (nombre, apellido, product_id[1], cantidad, payment, money, delivered, change)

			cursor.execute(add_sell, sell_items)
			mydb.commit()

			cursor.execute("SELECT @vuelto")
			pedir_vuelto = cursor.fetchall()
			vuelto = pedir_vuelto[0][0]

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()
			print(error_message)

		self.layout_insert_sell.layout_sell.label_change_total.setText(f"Vuelto: {vuelto}")
		self.layout_insert_sell.layout_sell.label_change_total.setHidden(False)
		self.layout_insert_sell.layout_sell.label_inserted.setHidden(False)

	################################
	####	Ventana Pendiente	####
	################################

	def terminate_sell_money(self):
		id = self.layout_pending.layout_pending_change.combo_box_id.currentText()
		print(id)
		id = float(int(id))
		lst_id = []
		lst_id.insert(0, id)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on terminate_sell_money")

			cursor.callproc('actualizar_pendientes_vuelto', lst_id)
			mydb.commit()

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

	def window_pending_money(self):
		self.layout_pending.layout_pending_change.tableWidget.setRowCount(0)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_pending_money")

			cursor.execute("SELECT `ID de la Venta` FROM cambios_pendientes")
			ids = cursor.fetchall()
			size_ids = len(ids)
			items = []
			for i in range(size_ids):
				items.insert(i, str(ids[i][0]))
			self.layout_pending.layout_pending_change.combo_box_id.addItems(items)

			cursor.execute("SELECT * FROM cantidad_columnas")
			cantidad_columnas = cursor.fetchall()
			column_size = int(cantidad_columnas[0][0])
			#cursor.reset()

			cursor.execute("SELECT * FROM cambios_pendientes")
			resultado = cursor.fetchall()

			self.layout_pending.layout_pending_change.tableWidget.setColumnCount(column_size)

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

		column_number = 0
		for row_number, row_data in enumerate(resultado):
			self.layout_pending.layout_pending_change.tableWidget.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				item =  QTableWidgetItem(str(data))
				item.setFlags(QtCore.Qt.ItemIsEnabled)
				self.layout_pending.layout_pending_change.tableWidget.setItem(row_number, column_number, item)
	
		self.layout_pending.layout_pending_change.tableWidget.setHorizontalHeaderLabels(header_columns)
		self.layout_pending.layout_pending_change.button_id.clicked.connect(self.terminate_sell_money)
	
	def window_pending_product(self):
		self.layout_pending.layout_pending_product.tableWidget.setRowCount(0)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_pending_product")

			cursor.execute("SELECT `ID de la Venta` FROM productos_pendientes")
			ids = cursor.fetchall()
			size_ids = len(ids)
			items = []
			for i in range(size_ids):
				items.insert(i, str(ids[i][0]))
			self.layout_pending.layout_pending_product.combo_box_id.addItems(items)

			cursor.execute("SELECT * FROM cantidad_columnas")
			cantidad_columnas = cursor.fetchall()
			column_size = int(cantidad_columnas[0][0])
			#cursor.reset()

			cursor.execute("SELECT * FROM productos_pendientes")
			resultado = cursor.fetchall()

			self.layout_pending.layout_pending_product.tableWidget.setColumnCount(column_size)

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

		column_number = 0
		for row_number, row_data in enumerate(resultado):
			self.layout_pending.layout_pending_product.tableWidget.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				item =  QTableWidgetItem(str(data))
				item.setFlags(QtCore.Qt.ItemIsEnabled)
				self.layout_pending.layout_pending_product.tableWidget.setItem(row_number, column_number, item)

		self.layout_pending.layout_pending_product.tableWidget.setHorizontalHeaderLabels(header_columns)
	
	def window_pending_all(self):
		self.layout_pending.layout_pending_all.tableWidget.setRowCount(0)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_pending_all")

			cursor.execute("SELECT `ID de la Venta` FROM todo_pendientes")
			ids = cursor.fetchall()
			size_ids = len(ids)
			items = []
			for i in range(size_ids):
				items.insert(i, str(ids[i][0]))
			self.layout_pending.layout_pending_all.combo_box_id.addItems(items)

			cursor.execute("SELECT * FROM cantidad_columnas")
			cantidad_columnas = cursor.fetchall()
			column_size = int(cantidad_columnas[0][0])
			#cursor.reset()

			cursor.execute("SELECT * FROM todo_pendientes")
			resultado = cursor.fetchall()

			self.layout_pending.layout_pending_all.tableWidget.setColumnCount(column_size)

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

		column_number = 0
		for row_number, row_data in enumerate(resultado):
			self.layout_pending.layout_pending_all.tableWidget.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				item =  QTableWidgetItem(str(data))
				item.setFlags(QtCore.Qt.ItemIsEnabled)
				self.layout_pending.layout_pending_all.tableWidget.setItem(row_number, column_number, item)

		self.layout_pending.layout_pending_all.tableWidget.setHorizontalHeaderLabels(header_columns)
	
	################################
	####	Ventana Registros	####
	################################
	
	def window_logs_all(self):
		self.layout_logs.layout_logs.table_widget.setRowCount(0)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_logs_all")

			cursor.execute("SELECT * FROM cantidad_columnas")
			cantidad_columnas = cursor.fetchall()
			column_size = int(cantidad_columnas[0][0])
			#cursor.reset()

			self.layout_logs.layout_logs.table_widget.setColumnCount(column_size)

			cursor.execute("SELECT * FROM Definitivo")
			resultado = cursor.fetchall()

			cursor.execute("SELECT * FROM bruto")
			bruto = cursor.fetchone()
			self.layout_logs.layout_logs.label_bruto.setText(f"Total Bruto: {bruto[0]}")
			print("Contenido de variable:",bruto)

			cursor.execute("SELECT * FROM neto")
			neto = cursor.fetchone()
			self.layout_logs.layout_logs.label_neto.setText(f"Total Neto: {neto[0]}")
			print("Contenido de hola:",neto)
			#self.layout_logs.layout_logs.label_total.setText(f"Bruto = {str(bruto)}")

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

		column_number = 0
		for row_number, row_data in enumerate(resultado):
			self.layout_logs.layout_logs.table_widget.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				item = QTableWidgetItem(str(data))
				item.setFlags(QtCore.Qt.ItemIsEnabled)
				self.layout_logs.layout_logs.table_widget.setItem(row_number, column_number, item)

		self.layout_logs.layout_logs.table_widget.setHorizontalHeaderLabels(header_columns)
	
	################################
	####	Ventana Productos	####
	################################
	
	def window_products_see(self):
		self.layout_products.layout_see.table_widget.setRowCount(0)

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_product_see")

			cursor.execute("SELECT * FROM cantidad_columnas_productos")
			cantidad_columnas = cursor.fetchall()
			column_size = int(cantidad_columnas[0][0])
			#cursor.reset()

			cursor.execute("SELECT * FROM Productos")
			resultado = cursor.fetchall()

			self.layout_products.layout_see.table_widget.setColumnCount(column_size)

			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()

		column_number = 0
		for row_number, row_data in enumerate(resultado):
			self.layout_products.layout_see.table_widget.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				item = QTableWidgetItem(str(data))
				item.setFlags(QtCore.Qt.ItemIsEnabled)
				self.layout_products.layout_see.table_widget.setItem(row_number, column_number, item)
	
	def clear_window_products_insert(self):
		self.layout_products.layout_insert.label_inserted.setHidden(True)
		self.layout_products.layout_insert.line_edit_product_name.setText("")
		self.layout_products.layout_insert.line_edit_product_description.setText("")
		self.layout_products.layout_insert.double_spin_box_product_price.setValue(0)

	def window_products_insert(self):
		name = self.layout_products.layout_insert.line_edit_product_name.text()
		name = name.capitalize()
		description = self.layout_products.layout_insert.line_edit_product_description.text()
		price = float(self.layout_products.layout_insert.double_spin_box_product_price.text())

		try:
			mydb = mysql.connector.connect(**connection_config_dict)
			cursor = mydb.cursor()
			print("You are on window_product_insert")

			add_product = "INSERT INTO Productos (nombre, descripcion, precio) VALUES (%s, %s, %s)"
			product = (name, description, price)
			cursor.execute(add_product, product)

			self.layout_products.layout_insert.label_inserted.setHidden(False)

			mydb.commit()
			cursor.close()
			mydb.close()
		except mysql.connector.Error as e:
			if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
				error_message = "Usuario o contraseña incorrecto para la base de datos."
			elif e.errno == errorcode.ER_BAD_DB_ERROR:
				error_message = "Base de datos inexitente."
			else:
				error_message = f"Error al conectarse a la base de datos: {e}"
			self.layout_error_db.label_error_mysql.setText(error_message)
			self.change_window_error_db()
		
		self.layout_products.layout_insert.button_new_product.clicked.connect(self.clear_window_products_insert)
	
app = QApplication()
window = window_sql()
window.show()
app.exec()