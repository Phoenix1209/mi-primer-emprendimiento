from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
	QMainWindow,
	QVBoxLayout,
	QPushButton,
	QTableWidget,
	QWidget,
	QFrame,
	QStackedLayout,
	QApplication,
	QHBoxLayout,
	QComboBox,
	QLabel
)

class window_pending (QFrame):
	def __init__(self):
		super().__init__()

		self.layout_pending = QVBoxLayout()

		self.label_title = QLabel("Ventana Pendientes")
		self.label_title.setAlignment(Qt.AlignCenter)
		self.button_pending_change = QPushButton("Dinero Pendiente")
		self.button_pending_product = QPushButton("Productos Pendientes")
		self.button_pending_all = QPushButton("Todo Pendiente")
		self.button_return = QPushButton("Regresar")

		list_widgets = [
			QLabel(),
			self.label_title,
			QLabel(),
			self.button_pending_change,
			QLabel(),
			self.button_pending_product,
			QLabel(),
			self.button_pending_all,
			QLabel(),
			self.button_return,
			QLabel()
		]

		for i in list_widgets:
			self.layout_pending.addWidget(i)

		self.setLayout(self.layout_pending)

class window_pending_money (QFrame):
	def __init__ (self):
		super().__init__()

		self.layout_pending_change = QVBoxLayout()

		self.layout_aux = QHBoxLayout()
		self.combo_box_id = QComboBox()
		self.button_id = QPushButton("Finalizar Pedido")
		self.layout_aux.addWidget(self.combo_box_id)
		self.layout_aux.addWidget(self.button_id)


		self.tableWidget = QTableWidget()
		self.button_return = QPushButton("Regresar")

		self.layout_pending_change.addLayout(self.layout_aux)
		self.layout_pending_change.addWidget(self.tableWidget)
		self.layout_pending_change.addWidget(self.button_return)

		self.setLayout(self.layout_pending_change)

class window_pending_product (QFrame):
	def __init__ (self):
		super().__init__()
		self.layout_pending_product = QVBoxLayout()

		self.layout_aux = QHBoxLayout()
		self.combo_box_id = QComboBox()
		self.button_id = QPushButton("Finalizar Pedido")
		self.layout_aux.addWidget(self.combo_box_id)
		self.layout_aux.addWidget(self.button_id)

		self.tableWidget = QTableWidget()
		self.button_return = QPushButton("Regresar")

		self.layout_pending_product.addLayout(self.layout_aux)
		self.layout_pending_product.addWidget(self.tableWidget)
		self.layout_pending_product.addWidget(self.button_return)

		self.setLayout(self.layout_pending_product)

class window_pending_all (QFrame):
	def __init__ (self):
		super().__init__()
		self.layout_pending_all = QVBoxLayout()

		self.layout_aux = QHBoxLayout()
		self.combo_box_id = QComboBox()
		self.button_id = QPushButton("Finalizar Pedido")
		self.layout_aux.addWidget(self.combo_box_id)
		self.layout_aux.addWidget(self.button_id)

		self.tableWidget = QTableWidget()
		self.button_return = QPushButton("Regresar")

		self.layout_pending_all.addLayout(self.layout_aux)
		self.layout_pending_all.addWidget(self.tableWidget)
		self.layout_pending_all.addWidget(self.button_return)

		self.setLayout(self.layout_pending_all)

class window_pending_main (QMainWindow):
	def __init__(self):
		super().__init__()

		self.layout_log = QStackedLayout()

		self.layout_pending_main = window_pending()
		self.layout_pending_change = window_pending_money()
		self.layout_pending_product = window_pending_product()
		self.layout_pending_all = window_pending_all()

		self.layout_log.addWidget(self.layout_pending_main)
		self.layout_log.addWidget(self.layout_pending_change)
		self.layout_log.addWidget(self.layout_pending_product)
		self.layout_log.addWidget(self.layout_pending_all)

		self.layout_pending_main.button_pending_change.clicked.connect(self.change_window_pending_money)
		self.layout_pending_main.button_pending_product.clicked.connect(self.change_window_pending_product)
		self.layout_pending_main.button_pending_all.clicked.connect(self.change_window_pending_all)

		self.layout_pending_change.button_return.clicked.connect(self.change_window_pending_main)
		self.layout_pending_product.button_return.clicked.connect(self.change_window_pending_main)
		self.layout_pending_all.button_return.clicked.connect(self.change_window_pending_main)

		self.widget = QWidget()
		self.widget.setLayout(self.layout_log)
		self.setCentralWidget(self.widget)
	
	def change_window_pending_main(self):
		self.layout_log.setCurrentIndex(0)
	
	def change_window_pending_money(self):
		self.layout_log.setCurrentIndex(1)
	
	def change_window_pending_product(self):
		self.layout_log.setCurrentIndex(2)
	
	def change_window_pending_all(self):
		self.layout_log.setCurrentIndex(3)

# app = QApplication()
# window = window_pending_main()
# window.show()
# app.exec()