from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
	QMainWindow,
	QGridLayout,
	QWidget,
	QApplication,
	QLabel,
	QLineEdit,
	QPushButton,
	QDoubleSpinBox,
	QSpinBox,
	QFrame,
	QVBoxLayout,
	QTableWidget,
	QStackedLayout
)

class window_main_product(QFrame):
	def __init__(self):
		super().__init__()

		self.layout_main = QVBoxLayout()

		self.label_title = QLabel("Ventana Productos")
		self.label_title.setAlignment(Qt.AlignCenter)
		self.button_see = QPushButton("Ver Productos Insertados")
		self.button_insert = QPushButton("Insertar Producto a la Base de Datos")
		self.button_return = QPushButton("Regresar")

		list_widgets = [
			QLabel(),
			self.label_title,
			QLabel(),
			self.button_see,
			QLabel(),
			self.button_insert,
			QLabel(),
			self.button_return,
			QLabel()
		]

		for i in list_widgets:
			self.layout_main.addWidget(i)

		self.setLayout(self.layout_main)

class window_products_see(QFrame):
	def __init__(self):
		super().__init__()

		self.layout_see = QVBoxLayout()

		self.table_widget = QTableWidget()
		self.button_return = QPushButton("Regresar")

		self.layout_see.addWidget(self.table_widget)
		self.layout_see.addWidget(self.button_return)

		self.setLayout(self.layout_see)

class window_insert_product(QFrame):
	def __init__(self):
		super().__init__()

		self.layout_window_insert_product = QGridLayout()

		self.label_product_name = QLabel("Nombre del Producto:")
		self.line_edit_product_name = QLineEdit()
		self.label_product_description = QLabel("Descripción del Producto:")
		self.line_edit_product_description = QLineEdit()
		self.label_product_price = QLabel("Precio del Producto:")
		self.double_spin_box_product_price = QDoubleSpinBox()
		self.label_inserted = QLabel("Producto insertado correctamente")

		self.button_new_product = QPushButton("Nuevo Producto")
		self.button_insert = QPushButton("Insertar Producto")
		self.button_return = QPushButton("Regresar")
		self.button_see_inserted = QPushButton("Ver Productos Insertados")

		list_widgets = [
			self.label_product_name,
			self.line_edit_product_name,
			self.label_product_description,
			self.line_edit_product_description,
			self.label_product_price,
			self.double_spin_box_product_price,
			QLabel(),
			QLabel(),
			self.label_inserted,
			QLabel(),
			self.button_new_product,
			self.button_insert,
			self.button_return,
			self.button_see_inserted
		]
		
		size_of_list = len(list_widgets)
		row = 0
		column = 0
		for i in range(size_of_list):
			if column == 2:
				column = 0
				row += 1
			self.layout_window_insert_product.addWidget(list_widgets[i], row, column)
			column += 1
		
		self.label_inserted.setHidden(True)
		
		self.setLayout(self.layout_window_insert_product)

class window_products(QMainWindow):
	def __init__(self):
		super().__init__()

		self.layout_products = QStackedLayout()

		self.layout_main = window_main_product()
		self.layout_see = window_products_see()
		self.layout_insert = window_insert_product()

		self.layout_products.addWidget(self.layout_main)
		self.layout_products.addWidget(self.layout_see)
		self.layout_products.addWidget(self.layout_insert)

		self.layout_main.button_see.clicked.connect(self.change_window_see)
		self.layout_main.button_insert.clicked.connect(self.change_window_insert)

		self.layout_see.button_return.clicked.connect(self.change_window_main)
		self.layout_insert.button_see_inserted.clicked.connect(self.change_window_see)
		self.layout_insert.button_return.clicked.connect(self.change_window_main)

		widget = QWidget()
		widget.setLayout(self.layout_products)
		self.setCentralWidget(widget)
	
	def change_window_main(self):
		self.layout_products.setCurrentIndex(0)
	
	def change_window_see(self):
		self.layout_products.setCurrentIndex(1)
	
	def change_window_insert(self):
		self.layout_products.setCurrentIndex(2)

# app = QApplication()
# window = window_products()
# window.show()
# app.exec()