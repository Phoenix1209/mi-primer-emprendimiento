from PySide6.QtWidgets import (
	QMainWindow,
	QVBoxLayout,
	QTableWidget,
	QPushButton,
	QWidget,
	QFrame,
	QLineEdit,
	QStackedLayout,
	QApplication,
	QLabel
)

class window_all_log (QFrame):
	def __init__(self):
		super().__init__()

		self.layout_all_log = QVBoxLayout()

		self.table_widget = QTableWidget()
		self.label_bruto = QLabel()
		self.label_neto = QLabel()
		self.button_return = QPushButton("Regresar")

		self.layout_all_log.addWidget(self.table_widget)
		self.layout_all_log.addWidget(self.label_bruto)
		self.layout_all_log.addWidget(self.label_neto)
		self.layout_all_log.addWidget(self.button_return)

		self.setLayout(self.layout_all_log)

class window_logs (QMainWindow):
	def __init__(self):
		super().__init__()
		self.layout_all_log = QStackedLayout()

		self.layout_logs = window_all_log()

		self.layout_all_log.addWidget(self.layout_logs)

		self.setLayout(self.layout_all_log)

		self.widget = QWidget()
		self.widget.setLayout(self.layout_all_log)
		self.setCentralWidget(self.widget)

	def change_window_main_log(self):
		self.layout_all_log.setCurrentIndex(0)
	
# app = QApplication()
# window = window_logs()
# window.show()
# app.exec()