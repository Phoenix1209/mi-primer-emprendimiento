from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
	QMainWindow,
	QVBoxLayout,
	QLabel,
	QPushButton,
	QWidget,
	QApplication
)

class window_error_mysql (QMainWindow):
	def __init__(self):
		super().__init__()

		self.layout_error_mysql = QVBoxLayout()

		self.label_title_error = QLabel("Error en la Base de Datos")
		self.label_title_error.setAlignment(Qt.AlignCenter)
		self.label_error_mysql = QLabel()
		self.button_exit = QPushButton("Entendido")

		list_widgets = [
			self.label_title_error,
			self.label_error_mysql,
			QLabel(),
			self.button_exit
		]

		for i in list_widgets:
			self.layout_error_mysql.addWidget(i)

		self.widget = QWidget()
		self.widget.setLayout(self.layout_error_mysql)
		self.setCentralWidget(self.widget)

# app = QApplication()
# window = window_error_mysql()
# window.show()
# app.exec()