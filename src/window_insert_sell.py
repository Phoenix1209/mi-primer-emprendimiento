from PySide6.QtWidgets import (
	QMainWindow,
	QGridLayout,
	QLabel,
	QLineEdit,
	QSpinBox,
	QComboBox,
	QPushButton,
	QWidget,
	QApplication,
	QDoubleSpinBox,
	QFrame,
	QStackedLayout,
	QCompleter,
	QCheckBox
)

class window_insertion(QFrame):
	def __init__(self):
		super().__init__()

		self.layout_insertion = QGridLayout()

		self.label_nombre = QLabel("Nombre")
		self.line_edit_nombre = QLineEdit()
		self.label_apellido = QLabel("Apellido")
		self.line_edit_apellido = QLineEdit()
		self.label_product_name = QLabel("Nombre del Producto: ")
		self.line_edit_product_name = QLineEdit()
		self.label_to_buy = QLabel("Cantidad a Comprar: ")
		self.spin_box_to_buy = QSpinBox()
		self.label_payment_method = QLabel("Método de Pago: ")
		self.combo_box_payment_method = QComboBox()
		self.label_received_money = QLabel("Cantidad de Dinero Recibida: ")
		self.double_spin_box_received_money = QDoubleSpinBox()
		self.label_product_delivered = QLabel("¿Producto Entregado?: ")
		self.check_box_product_delivered = QCheckBox()
		self.label_change_delivered = QLabel("¿Se entregó el Cambio?: ")
		self.check_box_change_delivered = QCheckBox()
		self.label_inserted = QLabel("Producto Insertado Correctamente")
		self.label_change_total = QLabel("Esconder")

		self.button_new_sell = QPushButton("Nueva Venta")
		self.button_insert = QPushButton("Realizar Venta")
		self.button_return = QPushButton("Regresar")
		self.button_logs = QPushButton("Registros")

		list_label = [
			self.label_nombre,
			self.line_edit_nombre,
			self.label_apellido,
			self.line_edit_apellido,
			self.label_product_name,
			self.line_edit_product_name,
			self.label_to_buy,
			self.spin_box_to_buy,
			self.label_payment_method,
			self.combo_box_payment_method,
			self.label_received_money,
			self.double_spin_box_received_money,
			self.label_product_delivered,
			self.check_box_product_delivered,
			self.label_change_delivered,
			self.check_box_change_delivered,
			self.label_inserted,
			self.label_change_total,
			self.button_new_sell,
			self.button_insert,
			self.button_return,
			self.button_logs
		]

		size_of_list = len(list_label)

		row = 0
		column = 0
		for i in range(size_of_list):
			if column == 2:
				column = 0
				row += 1
			self.layout_insertion.addWidget(list_label[i], row, column)
			column += 1

		self.check_box_product_delivered.setChecked(True)
		self.check_box_change_delivered.setChecked(True)
		self.label_inserted.setHidden(True)
		self.label_change_total.setHidden(True)

		self.setLayout(self.layout_insertion)

class window_insert_sell (QMainWindow):
	def __init__(self):
		super().__init__()

		self.layout_insert_sell = QStackedLayout()

		self.layout_sell = window_insertion()

		self.layout_insert_sell.addWidget(self.layout_sell)

		self.widget = QWidget()
		self.widget.setLayout(self.layout_insert_sell)
		self.setCentralWidget(self.widget)

# app = QApplication()
# window = window_insert_sell()
# window.show()
# app.exec()