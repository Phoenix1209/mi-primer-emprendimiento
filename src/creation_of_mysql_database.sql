####################################
####	Creation of DataBase	####
####################################

use sys;

DROP DATABASE IF EXISTS primer_emprendimiento;

CREATE DATABASE primer_emprendimiento;

USE primer_emprendimiento;

####################################
####	Creations of Tables		####
####################################

CREATE TABLE Productos (
	id_producto int UNSIGNED AUTO_INCREMENT,
	nombre varchar (64) NOT NULL,
	descripcion varchar (255),
	precio decimal (10,2) NOT NULL,
	
	CONSTRAINT Productos_id_producto_pk PRIMARY KEY (id_producto)
);

CREATE TABLE Registros (
	id_registro int UNSIGNED AUTO_INCREMENT,
	nombre_cliente varchar(32),
	apellido_cliente varchar(32),
	id_producto int UNSIGNED,
	cantidad TINYINT UNSIGNED,
	total decimal (10,2),
	tipo_pago varchar (16) NOT NULL,
	pagado decimal (10,2),
	vuelto decimal (10,2),
	producto_entregado char (2) NOT NULL,
	cambio_entregado char (2) NOT NULL,
	fecha_hora timestamp DEFAULT current_timestamp,
	
	CONSTRAINT Registros_id_registro_pk PRIMARY KEY (id_registro),
	CONSTRAINT Registros_tipo_pago_ck CHECK (
		tipo_pago IN ('Efectivo', 'Yappy', 'Al Contado')
	),
	CONSTRAINT Registros_producto_entregado_ck CHECK (
		producto_entregado IN ("Sí", "No")
	),
	CONSTRAINT Registros_cambio_entregado_ck CHECK (
		cambio_entregado IN ('Sí', 'No')
	),
	CONSTRAINT Pedidos_id_producto_fk FOREIGN KEY (id_producto)
		REFERENCES Productos (id_producto)
);

########################################
####	Creations of Procedures		####
########################################

DELIMITER |
CREATE PROCEDURE actualizar_pendientes_vuelto (
	IN id int
) BEGIN
	UPDATE Registros SET
		cambio_entregado = "Sí"
	WHERE id_registro = id;
END;
|
DELIMITER ;

DELIMITER |
CREATE PROCEDURE actualizar_pendientes_producto (
	IN id int
) BEGIN
	UPDATE Registros SET
		producto_entregado = "Sí"
	WHERE id_registro = id;
END;
|
DELIMITER ;

DELIMITER |
CREATE PROCEDURE actualizar_pendientes_todo (
	IN id int
) BEGIN
	UPDATE Registros SET
		producto_entregado = "Sí",
		cambio_entregado = "Sí"
	WHERE id_registro = id;
END;
|
DELIMITER ;

DELIMITER |
CREATE PROCEDURE obtener_producto_id (
	IN nombre_producto varchar(64),
	OUT id int
) BEGIN
	SELECT	id_producto
	INTO	id
	FROM	Productos
	WHERE	Productos.nombre = nombre_producto;
END;
|
DELIMITER ;

####################################
####	Creations of Triggers	####
####################################

CREATE TRIGGER Calcular_total BEFORE INSERT ON Registros
FOR EACH ROW SET NEW.total = (
	SELECT Productos.precio * NEW.cantidad
	FROM	Productos
	WHERE Productos.id_producto = NEW.id_producto
);

CREATE TRIGGER Calcular_vuelto BEFORE INSERT ON Registros
FOR EACH ROW SET NEW.vuelto = NEW.pagado - NEW.total;

CREATE TRIGGER obtener_vuelto BEFORE INSERT ON Registros
FOR EACH ROW SET @vuelto = NEW.vuelto;

SET @bruto = 0.00;
CREATE TRIGGER total_bruto AFTER INSERT ON Registros
FOR EACH ROW SET @bruto = @bruto + NEW.total;

SET @neto = 0.00;
DELIMITER |
CREATE TRIGGER total_neto AFTER INSERT ON Registros
FOR EACH ROW BEGIN
	IF NEW.cambio_entregado = "Sí" THEN
		SET @neto = @neto + NEW.total;
	END IF;
END;
|
DELIMITER ;


# Crear trigger que llame a un procedimiento o funcion que actualice el vuelto negativo

####################################
####	Creations of Views		####
####################################

# Muestra el regristro más importante
CREATE OR REPLACE VIEW Definitivo AS
SELECT	Registros.id_registro AS "ID de la Venta",
		Registros.nombre_cliente AS "Nombre",
		Registros.apellido_cliente AS "Apellido",
		Productos.nombre AS Producto, #####################
		Productos.precio AS "Precio Individual",
		Registros.cantidad AS Cantidad, #######################
		Registros.total AS Total,
		Registros.tipo_pago AS "Tipo de Pago", ########################
		Registros.pagado AS Pagado, ##############################
		Registros.vuelto AS Vuelto,
		Registros.producto_entregado AS "Producto Entregado", #########################
		Registros.cambio_entregado AS "Cambio Entregado",
		TIME(Registros.fecha_hora) AS "Hora",
		DATE(Registros.fecha_hora) AS "Fecha"
FROM Registros
	JOIN Productos ON Productos.id_producto = Registros.id_producto
ORDER BY 1 DESC;

# Muestra la cantidad de columnas de la vista Definitivo
CREATE OR REPLACE VIEW cantidad_columnas AS
SELECT	CAST(count(COLUMN_NAME) AS char)
FROM	INFORMATION_SCHEMA.COLUMNS
WHERE	table_name = 'Definitivo' AND
		table_schema = 'primer_emprendimiento';

# Muestra la cantidad de columnas de la tabla Productos
CREATE OR REPLACE VIEW cantidad_columnas_productos AS
SELECT	CAST(count(COLUMN_NAME) AS char)
FROM	INFORMATION_SCHEMA.COLUMNS
WHERE	table_name = 'Productos' AND
		table_schema = 'primer_emprendimiento';

# Muestra de la vista Definitivo los registros con entrega de productos pendientes
CREATE OR REPLACE VIEW productos_pendientes AS
SELECT	*
FROM	Definitivo
WHERE	`Producto Entregado` = "No"
ORDER BY 1 ASC;

# Muestra de la vista Definitivo los registros con entrega de dinero pendientes
CREATE OR REPLACE VIEW cambios_pendientes AS
SELECT	*
FROM	Definitivo
WHERE	Definitivo.`Cambio Entregado` = "No"
ORDER BY 1 ASC;

# Muestra de la vista Definitivo los registros con entrega de productos y dinero pendientes
CREATE OR REPLACE VIEW todo_pendientes AS
SELECT	*
FROM	Definitivo
WHERE	Definitivo.`Cambio Entregado` = "No" OR
		Definitivo.`Producto Entregado` = "No"
ORDER BY 1 ASC;

# Muestra el salario Bruto
CREATE OR REPLACE VIEW bruto AS
SELECT	sum(total)
FROM	Registros;

# Muestra el salario Neto
CREATE OR REPLACE VIEW neto AS
SELECT	sum(total)
FROM	Registros
WHERE	cambio_entregado = "Sí";

################################
####	SELECTS & CALLS		####
################################

# Verificar si es la primera vez que el programa se ejecuta
SET @nombre_empresa = 0;

/*INSERT INTO Productos (nombre, descripcion, precio) VALUES
	("Duro", "Es frío y en bolsa", 0.35),
	("Pastelitos", "Se come xd", 0.65);

INSERT INTO Registros (id_producto, cantidad, tipo_pago, pagado, producto_entregado, cambio_entregado) VALUES
	(1, 1, "Efectivo", 0, "Sí", "No"),
	(2, 1, "Efectivo", 0, "No", "Sí");*/

-- SELECT	@id_producto_solicitado;
-- 
-- SELECT * FROM Productos;
-- SELECT * FROM Registros;
-- 
-- SELECT * FROM Definitivo;
-- SELECT * FROM cantidad_columnas;
-- SELECT * FROM cantidad_columnas_productos;
-- SELECT * FROM productos_pendientes;
-- SELECT * FROM cambios_pendientes;
-- 
-- SHOW FULL TABLES IN primer_emprendimiento
-- WHERE table_type = 'VIEW';
-- 
-- SELECT @nombre_empresa;
-- 
-- SELECT *
-- FROM Definitivo
-- WHERE Definitivo.Nombre IS NULL;

SELECT * FROM neto;
SELECT * FROM bruto;
SELECT @vuelto;


