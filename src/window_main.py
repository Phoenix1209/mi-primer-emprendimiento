from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
	QMainWindow,
	QVBoxLayout,
	QWidget,
	QPushButton,
	QApplication,
	QLabel
)

class window_main (QMainWindow):
	def __init__(self):
		super().__init__()

		self.layout_main = QVBoxLayout()

		self.label_name = QLabel("Mi Primer Emprendimiento")
		self.label_name.setAlignment(Qt.AlignCenter)
		self.button_insert_sell = QPushButton("Nueva Venta")
		self.button_pending = QPushButton("Pendiente")
		self.button_all_log = QPushButton("Registros")
		self.button_products = QPushButton("Productos")
		self.button_export_db = QPushButton("Exportar Base de Datos")
		self.button_exit = QPushButton("Salir")

		list_layout_main = [
			QLabel(),
			self.label_name,
			QLabel(),
			self.button_insert_sell,
			QLabel(),
			self.button_pending,
			QLabel(),
			self.button_all_log,
			QLabel(),
			self.button_products,
			QLabel(),
			self.button_export_db,
			QLabel(),
			self.button_exit,
			QLabel()
		]

		for w in list_layout_main:
			self.layout_main.addWidget(w)

		self.widget = QWidget()
		self.widget.setLayout(self.layout_main)
		self.setCentralWidget(self.widget)

# app = QApplication()
# window = window_main()
# window.show()
# app.exec()